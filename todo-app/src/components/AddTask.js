import { useState } from "react"

const AddTaskForm = (props) => {
    const [ input, setInput ] = useState('')


    const handleChange = e => {
        setInput(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        const createId = () =>{
            return Math.floor(Math.random() * 10000000)
        }
        const uppercaseToInput = `${input[0].toUpperCase()}${input.slice(1)}`
        props.onSubmit({
            id: createId(),
            title: uppercaseToInput,
            isComplete: false
        })
        
        setInput('')
    }

    return (
        <>
        <form className="task-form" onSubmit={handleSubmit}>
          <div className="row">
            <div className="input-field" >

                <input
                    type="text"
                    placeholder="Add task"
                    value={input}
                    name="text"
                    className="validate col s10 m6 task-input"
                    onChange={handleChange}/>

                <button
                    className="btn-small waves-effect waves-light col s2 m1 task-button"
                    name="action">

                <i className="material-icons">send</i>
                </button>
                </div>

          </div>
        </form>
      </>
    )
}

export default AddTaskForm