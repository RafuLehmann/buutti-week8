import './App.css';

import TodoList from './components/taskList';


function App() {
  return (

      <div className="App">
        <>
        <h1>Daily tasks</h1> 
        </>
        <div className="container">
          <TodoList />
        </div>
      </div>
  );
}

export default App;