import { useState } from "react"
import AddTaskForm from "./AddTask.js"
// import TodoForm from "./TodoForm"

const Task = ({taskList, completeTask, deleteTask, editTask}) => {
    const [edit, setEdit] = useState({
        id: null,
        title: '',
        isComplete: false
    })

    const submitUpdate = (title) => {
        console.log(title.title)
        editTask(edit.id, title)
        setEdit({
            id: null,
            title: '',
            isComplete: false
        })
    }

    if (edit.id !== null) {
        return (
            
                <div class="left-align modify">
                    <h2>Modify task</h2>
                    <p>Previous title: {edit.title}</p>
                    <AddTaskForm edit={edit} onSubmit={submitUpdate} />
                </div>
            )
    }

    return taskList.map((task, index) => (
        <li 
            className=
                { 
                    task.isComplete ?   
                    'collection-item complete': 
                    'collection-item'
                } 
            key={index}
        >

            <div className="left-align" key={task.id}>

                {task.title}

                <span className="secondary-content" >
                    <i  id={task.id} className="material-icons"
                        onClick={() => deleteTask(task.id)}>
                        clear
                    </i>
                    <i  className="material-icons task-icon"
                        onClick={() => setEdit({id: task.id, title: task.title, isComplete: task.isComplete})}>
                        border_color
                    </i>
                    <i  className="material-icons task-icon" 
                        onClick={() => completeTask(task.id)}
                       >
                        { 
                            task.isComplete ?   
                            'check_circle': 
                            'check_box_outline_blank'
                        } 
                    </i>
                </span>
            </div>
        </li>
    ))
}

export default Task