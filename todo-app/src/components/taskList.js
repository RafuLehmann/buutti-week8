import { useState } from "react";

import Task from "./Task";
import AddTaskForm from "./AddTask.js";


const TodoList = () => {
    const [ taskList, setTodoList ] = useState([])


    const addTask = task => {
        if (!task.title) return
        setTodoList([task, ...taskList])
    }

    const deleteTask = id => {
        setTodoList([...taskList].filter(task => task.id !== id))
      }

    const completeTask = id => {
        let editedList = taskList.map(task => {
            if (task.id === id) {
                if (!task.isComplete) task.isComplete = true
                else if (task.isComplete) task.isComplete = false
            }
            return task
        })
        
        setTodoList(editedList.sort((a, b) => (a.isComplete > b.isComplete) ? 1 : -1))
    }

    const editTask = (id, newTitle) => {
        if (!newTitle.title) return

        const editedList = taskList.map(oldTitle => {
            if (oldTitle.id === id) return newTitle
            else return oldTitle
        }
            )

        setTodoList(editedList)
    }

    return (
        <>
            <AddTaskForm onSubmit={addTask} />
            <ul className="collection">
                <Task 
                    taskList={taskList} 
                    completeTask={completeTask} 
                    deleteTask={deleteTask} 
                    editTask={editTask} 
                    />
            </ul>
        </>
    )
}

export default TodoList